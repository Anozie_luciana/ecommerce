import connect from "./utils/connect"
import express from "express"
import config from "config"
import logger from "./utils/logger"

const port = config.get<number>("port")

const app = express()

app.listen(port, async()=>{
  logger.info(`listening to port: ${port}`)
  await connect()
})
