import mongoose from "mongoose";
import bcrypt from 'bcrypt'
import config from 'config'


export interface UserDocumenent extends mongoose.Document{
  email:string,
  password: string,
  name: string,
  createdAt: Date,
  updatedAt : Date
}

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type:String,
    required: true,
    unique: true
  },
  password:{
    type: String,
    required: true
  }
},{timestamps: true})


// userSchema.pre("save", async function(next: mongoose.HookNextFunction){
//  let user = this as UserDocumenent
// })

const User = mongoose.model("user", userSchema)

export default User