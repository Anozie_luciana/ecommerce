
import mongoose from "mongoose";
import config from 'config'
import logger from "./logger";

function connect(){
  const dbUri = config.get<string>("db")

  mongoose.set('strictQuery', false);

  return mongoose.connect(dbUri, 
    ).then(()=>{
   logger.info("connected to database")
  }).catch((error)=>{
    logger.error("failed to connect to database")
    process.exit(1)
  })
}



export default connect