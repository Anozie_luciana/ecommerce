import pino from 'pino'
import dayjs from 'dayjs'


// const log = pino({
//   prettyPrint: true,
//   base: {
//     //pid is the process id
//     pid:false,
//   },
//   timestamp:()=> `, "time" : "${dayjs().format()}"`
// })

const pretty = require('pino-pretty')
const stream = pretty({
  colorize: true
})
const logger = pino(stream)

logger.info('hi')


export default logger